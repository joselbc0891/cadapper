# Py Arquitectura Limpia en cSharp y Dapper

Este es un proyecto base para implementar arquitectura limpia, esta realizado con C# , MicroORM Dapper y una webapi con swagger
## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

```
git init
git clone https://gitlab.com/joselbc0891/cadapper

```
### Pre-requisitos 📋

Que cosas necesitas para ver el proyecto.

```
Visual Studio Comunity o Visual Studio Code
SqlServer

```
### Lo que contiene 📋
El proyecto contiene 3 capas
```
- Infraestructura
    -Data
    -IoC
- Core
    -Contracts
    -DTOs
    -Entities
    -IoC
    -UseCases
- WepApi
    -Controllers

```
La tabla de ejemplo estan aqui -> [Tabla ejemplo](https://gitlab.com/joselbc0891/cadapper/-/wikis/Tabla-de-ejemplo)

---
⌨️ con ❤️ por JBovadilla 😊
