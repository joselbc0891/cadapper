﻿using Microsoft.Extensions.DependencyInjection;
using AcrLimpia.Core.Contracts;
using AcrLimpia.Infrastructure.Data;

namespace AcrLimpia.Infrastructure.IoC
{
    public static class ServiceModuleExtentions
    {
        public static void RegisterInfrastructureServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUnitOfWork, UnitOfWork>();
            serviceCollection.AddTransient<IUsuarioRepository, UsuarioRepository>();
        }
    }
}