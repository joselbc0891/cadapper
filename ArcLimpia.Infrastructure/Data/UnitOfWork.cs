﻿using AcrLimpia.Core.Contracts;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace AcrLimpia.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private IServiceProvider _serviceProvider;

        public UnitOfWork(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
        }

        public IUsuarioRepository UsuarioRepository => _serviceProvider.GetService<IUsuarioRepository>();
    }
}