﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using AcrLimpia.Core.Contracts;
using AcrLimpia.Core.Entities;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace AcrLimpia.Infrastructure.Data
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(IConfiguration configuration) : base(configuration)
        {
        }
        public async Task<int> insertar(Usuario entidad)
        {

            using (SqlConnection cnx = new SqlConnection(_connectionString))
            {
                var arg = new DynamicParameters();
                arg.Add("@ID", entidad.ID);
                arg.Add("@NOMBRE", entidad.NOMBRE);
                arg.Add("@APELLIDOS", entidad.APELLIDOS);
                arg.Add("@ESTADO", entidad.ESTADO);
                cnx.Open();
                var result = await cnx.ExecuteAsync("dbo.insertarUsuario", arg, commandType: CommandType.StoredProcedure);
                return result;

            }
        }
        public async Task actualizar(Usuario entidad)
        {
            using (SqlConnection cnx = new SqlConnection(_connectionString))
            {
                var arg = new DynamicParameters();
                arg.Add("@ID", entidad.ID);
                arg.Add("@NOMBRE", entidad.NOMBRE);
                arg.Add("@APELLIDOS", entidad.APELLIDOS);
                arg.Add("@ESTADO", entidad.ESTADO);
                cnx.Open();
                await cnx.ExecuteAsync("dbo.actualizarUsuario", arg, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<List<Usuario>> listar()
        {
            using (SqlConnection cnx = new SqlConnection(_connectionString))
            {
                var arg = new DynamicParameters();
                cnx.Open();
                var retorno = await cnx.QueryAsync<Usuario>("dbo.listarUsuario", arg, commandType: CommandType.StoredProcedure);
                return retorno.ToList();
            }
        }
        public async Task<Usuario> obtener(Usuario entidad)
        {
            using (SqlConnection cnx = new SqlConnection(_connectionString))
            {
                var arg = new DynamicParameters();
                arg.Add("@ID", entidad.ID);
                cnx.Open();
                var retorno = await cnx.QueryAsync<Usuario>("dbo.obtenerUsuario", arg, commandType: CommandType.StoredProcedure);
                return retorno.FirstOrDefault();
            }
        }
        public async Task eliminar(Usuario entidad)
        {
            using (SqlConnection cnx = new SqlConnection(_connectionString))
            {
                var arg = new DynamicParameters();
                arg.Add("@ID", entidad.ID);
                cnx.Open();
                await cnx.ExecuteAsync("dbo.eliminarUsuario", arg, commandType: CommandType.StoredProcedure);
            }

        }
    }
}



