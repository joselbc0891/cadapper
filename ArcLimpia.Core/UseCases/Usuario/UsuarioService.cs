﻿using AcrLimpia.Core.Contracts;
using AcrLimpia.Core.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AcrLimpia.Core.UseCases
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUnitOfWork _uow;
        public UsuarioService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task actualizar(Usuario entidad)
        {
           await _uow.UsuarioRepository.actualizar(entidad);
        }

        public async Task eliminar(Usuario entidad)
        {
            await _uow.UsuarioRepository.eliminar(entidad);
        }

        public async Task<int> insertar(Usuario entidad) {
           var retorno = await _uow.UsuarioRepository.insertar(entidad);
           return retorno;
        }

        public async Task<List<Usuario>> listar()
        {
            var retorno = await _uow.UsuarioRepository.listar();
            return retorno;
        }

        public async Task<Usuario> obtener(Usuario entidad)
        {
            var retorno = await _uow.UsuarioRepository.obtener(entidad);
            return retorno;
        }
    }
}