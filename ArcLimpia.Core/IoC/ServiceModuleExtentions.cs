﻿using Microsoft.Extensions.DependencyInjection;
using AcrLimpia.Core.Contracts;
using AcrLimpia.Core.UseCases;

namespace AcrLimpia.Core.IoC
{
    public static class ServiceModuleExtentions
    {
        public static void RegisterCoreServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUsuarioService, UsuarioService>();
        }
    }
}
