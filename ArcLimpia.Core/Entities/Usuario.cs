﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AcrLimpia.Core.Entities
{
    public class Usuario : EntityBase
    {
        public string NOMBRE { get; set; }
        public string APELLIDOS { get; set; }
    }
}
