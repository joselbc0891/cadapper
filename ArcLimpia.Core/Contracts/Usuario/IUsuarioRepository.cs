﻿using AcrLimpia.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AcrLimpia.Core.Contracts
{
    public interface IUsuarioRepository : IAsyncGenericRepository<Usuario>
    {
        Task<int> insertar(Usuario entidad);
        Task actualizar(Usuario entidad);
        Task<List<Usuario>> listar();
        Task<Usuario> obtener(Usuario entidad);
        Task eliminar(Usuario entidad);
    }
}
