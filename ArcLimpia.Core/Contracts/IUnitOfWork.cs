﻿using System;

namespace AcrLimpia.Core.Contracts
{
    public interface IUnitOfWork
    {
        IUsuarioRepository UsuarioRepository { get; }
    }
}
