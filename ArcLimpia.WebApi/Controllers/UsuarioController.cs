﻿using Microsoft.AspNetCore.Mvc;
using AcrLimpia.Core.Contracts;
using AcrLimpia.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArcLimpia.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        protected readonly IUsuarioService _usuarioservice;
        public UsuarioController(IUsuarioService usuarioservice)
        {
            _usuarioservice = usuarioservice;
        }

        [HttpPost("insertar")]
        public async Task<IActionResult> insertar(string Nombre,string Apellido,string Estado)
        {
            Usuario args = new Usuario();
            args.NOMBRE = Nombre;
            args.APELLIDOS = Apellido;
            args.ESTADO = Estado;
            var result = await _usuarioservice.insertar(args);
            return Ok(result);
        }

        [HttpPost("actualizar")]
        public async Task<IActionResult> actualizar(string Nombre, string Apellido, string Estado)
        {
            Usuario args = new Usuario();
            args.NOMBRE = Nombre;
            args.APELLIDOS = Apellido;
            args.ESTADO = Estado;
            await _usuarioservice.actualizar(args);
            return Ok();
        }

        [HttpGet("obtener")]
        public async Task<IActionResult> obtener(int Id)
        {
            Usuario args = new Usuario();
            args.ID = Id;
            var result = await _usuarioservice.obtener(args);
            return Ok(result);
        }

        [HttpGet("listar")]
        public async Task<IActionResult> listar()
        {
            var result = await _usuarioservice.listar();
            return Ok(result);
        }

        [HttpPost("eliminar")]
        public async Task<IActionResult> eliminar(int Id)
        {
            Usuario args = new Usuario();
            args.ID = Id;
            await _usuarioservice.eliminar(args);
            return Ok();
        }
    }
}
